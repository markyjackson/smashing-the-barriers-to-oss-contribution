Please feel free to contribute to this guide and help improve it. 

Contributors must read, follow, and respect the Code of Conduct this project uses, which is the CNCF Code of Conduct. This is available for viewing in multiple languages at: https://github.com/cncf/foundation/blob/master/code-of-conduct.md 

If you don't know how to submit a pull request, or have never worked on OSS before, please ping @celanthe here on GitLab, and they will be happy to help you get set up, and are your primary point of contact for any questions you have regarding contributions to this project.

### Ways You Can Contribute: 

* Add your own ideas, thoughts, and best practices to the guide
* Ask questions and give helpful feedback
* Share this guide with others in your own OSS organization
* Help improve grammar, phrasing, or the layout of this guide
