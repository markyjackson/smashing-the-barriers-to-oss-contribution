# Obstacles Facing First-Time Contributors

* "Good first issue," that is really just, "Can someone else," and no help when you ask.
*  Hard to find good/"easy" first contributions
*  No clear description of the help the project needed (this can be an even bigger challenge for detail-oriented people)
*  Getting a grasp of the project, especially with large projects.
*  Lack of resources if something goes wrong in setup