# Smashing the Barriers to OSS Contribution
## A Guide to Improving the Contributor Experience for Neurodivergent Individuals

### Project Goals:

*  This guide is intended for those involved in open source software to help address some common issues those that are neurodivergent face when contributing to OSS.
    *  These issues were identified by participants in 'Smashing the Barriers to OSS Contribution,' which was a workshop given by Rin Oliver at Mozilla Festival 2019. Slides from this presentation are available for viewing at: https://www.slideshare.net/CKRinOliver/smashing-the-barriers-to-oss-contribution-187356432
    *  The issues listed here represent the lived experiences and opinions of those present during the workshop, which encompassed both neurodivergent and neurotypical attendees. 
        *  These are not intended to be representative (nor can they possibly be) of every individual neurodivergent person's lived experience. 
         
 * Please be kind, as this guide is a collaborative work in progress.

### What You'll Gain from Reading:

* A better understanding of how your organization can improve its overall contributor experience for those that are neurodivergent
* Understanding of accessibility issues that neurodivergent people face when contributing to OSS

### Guide Segments

* [Issues Relating to Lack of/Unclear Feedback](https://gitlab.com/celanthe/smashing-the-barriers-to-oss-contribution/blob/master/feedback-challenges.md)
* [Obstacles Facing First-Time Contributors](https://gitlab.com/celanthe/smashing-the-barriers-to-oss-contribution/blob/master/obstacles-to-contributing.md)
* [Issues Experienced by Contributors Both Neurodivergent and Neurotypical](https://gitlab.com/celanthe/smashing-the-barriers-to-oss-contribution/blob/master/initial-challenges.md)
* [Welcoming Open Source Contributions -- What to Consider](https://gitlab.com/celanthe/smashing-the-barriers-to-oss-contribution/blob/master/what-to-consider.md)
* [Helpful Things to Implement](https://gitlab.com/celanthe/smashing-the-barriers-to-oss-contribution/blob/master/helpful-things.md)
* [Proposed Structure and Solutions to Improve the Open Source Software Contributor Experience](https://gitlab.com/celanthe/smashing-the-barriers-to-oss-contribution/blob/master/structure-and-solutions.md)
