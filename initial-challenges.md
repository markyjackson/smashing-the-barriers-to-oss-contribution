# Issues Currently Facing New OSS Contributors Both Neurodivergent and Neurotypical
### These issues were written by participants in the 'Smashing the Barriers to OSS Contribution,' workshop that took place on 10/27/2019 at Mozilla Festival:

* Open source is hard to get into. As the ‘hows’ are not very clear, and neurodivergent people may not understand the process that easily.
* Lack of documentation on how to get started or set up.
* No time or money to do free work and to learn new skills.
* No localised documentation.
* ‘Most of the way’ work gets rejected, so putting in effort doesn’t feel appreciated.
* Getting into arguments because the language is either too complex, or confusions don’t get cleared up.
* Having to register to a website–Which name to put there?
* Unclear social norms for asking about/learning about project