# Helpful Things to Implement

* Instructions need to be given one at a time.
* Having one point of contact/a mentor
* Don't expect people to know all the rules, even if they're present online somewhere. Instead, help point people to docs welcomingly.
* Clear structure/schedule
* Offering help submitting a contribution
* Socially facilitating/speaking in front of others (for those interested in doing so) --Finding 'your people' is key