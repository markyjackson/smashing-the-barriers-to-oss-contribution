# Welcoming Open Source Contributions -- What to Consider:

* Don't use jargon
* Have a glossary
* Don't always assume people know what you mean
* Consider accessibility needs (screen readers, captioning, font readability, etc.)
* Use positive language
* Languages and cultures are diverse. Respect this!
* Have designated mentors that contributors or those considering contributing can go to with questions or concerns
* Have clear expectations of what you would like contributions to be comprised of
* Have neurodivergent mentors if possible on your project's team
* Keep in mind all neurodivergent people are different. If you've met one neurodivergent person, you have, in fact--Met only *one* neurodivergent person.
* Often we think of only hiring/including people with a good/"easy" community fit, but remember this can mean nondisruptive but different people