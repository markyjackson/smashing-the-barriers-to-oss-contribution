# Issues Relating to Lack of/Unclear Feedback

* Not confident that my contribution has value. Perfectionism.
* Being unsure if contribution is going to be evaluated and having to find out on top of this who to ask about that.
* LACK OF FEEDBACK on contribution. Didn't know if I was doing well or not. Positive feedback is important for continued contribution.
* Not clear what contribution is welcome, or even if any are welcome.
* Finding out where to report a bug/solution (if neurodivergent, having to expose this difficulty can be an issue)
* Finding out how to start contributing