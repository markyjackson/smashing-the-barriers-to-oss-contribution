# How Can We Better Structure the Open Source Software Contributor Experience?

* Clearer documentation
* Catering to all accessibility challenges
* Be up front
* Have an open and inclusive environment 

## Proposed Solutions for Improving the Contributor Experience for Neurodivergent Individuals:

* Better pathways to contributing
* Consider all abilities (beginner, intermediate, expert)
* Not everyone comes from a Computer Science background. Make space for those from non-traditional backgrounds.
* Detailed contributing guidelines
* Required Reading: Code of Conduct
* Identify New Contributor's assigned Point of Contact for any questions/concerns 
* **Needed:** More advocates and mentors